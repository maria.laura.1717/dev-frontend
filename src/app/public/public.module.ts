import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';

import { PublicRoutingModule } from './public-routing.module';
import { PublicSignUpComponent } from './public-sign-up/public-sign-up.component';
import {FormsModule} from "@angular/forms";


@NgModule({
  declarations: [
    PublicSignUpComponent
  ],
  imports: [
    CommonModule,
    FormsModule,
    PublicRoutingModule
  ]
})
export class PublicModule { }
