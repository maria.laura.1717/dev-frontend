import { NgModule } from '@angular/core';
import { RouterModule, Routes } from '@angular/router';
import {PublicSignUpComponent} from "./public-sign-up/public-sign-up.component";
import {DevLoginGuard} from "../shared/guards/dev-login.guard";


const routes: Routes = [
  {
    path: 'public-sign-up', component: PublicSignUpComponent,
    canActivate: [DevLoginGuard]
  }
];

@NgModule({
  imports: [RouterModule.forChild(routes)],
  exports: [RouterModule]
})
export class PublicRoutingModule { }
