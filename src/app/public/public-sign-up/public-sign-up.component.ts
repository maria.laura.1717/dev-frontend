import {Component, OnInit} from '@angular/core';
import {DevUserInput} from "../../integration/models/input/dev-user-input";
import {NgForm} from "@angular/forms";
import {DevCreateUserHttpService} from "../../integration/service/dev-create-user-http.service";
import {Router} from "@angular/router";
import {DevCreateBulletinHttpService} from "../../integration/service/dev-create-bulletin-http.service";

@Component({
  selector: 'app-sign-up-form',
  templateUrl: './public-sign-up.component.html',
  styleUrls: ['./public-sign-up.component.scss']
})
export class PublicSignUpComponent implements OnInit {
  public user: DevUserInput;

  constructor(private restUserService: DevCreateUserHttpService,
              private restNewsService: DevCreateBulletinHttpService,
              private router: Router) {
    this.user = new DevUserInput();
  }

  ngOnInit(): void {
  }

  public submit(childForm: NgForm): void {
    this.restUserService.add(this.user).subscribe(
      resp => {
        if(resp.status === 200) {
          alert('Succesful Registration');
          this.router.navigate(['/secure/news']);
          localStorage.setItem("UserId", resp.body.id);
        }
      },
      error => {
        if(error.status === 400){
          alert(error.error.message);
        }
      }
    );
  }
}
