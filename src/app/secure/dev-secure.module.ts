import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';

import { DevSecureRoutingModule } from './dev-secure-routing.module';
import {FormsModule} from "@angular/forms";
import {DevNewsComponent} from "./news/dev-news.component";
import { DevBulletinComponent } from './news/bulletin/dev-bulletin.component';
import { DevBulletinsComponent } from './news/bulletins/dev-bulletins.component';
import {DevSharedModule} from "../shared/dev-shared.module";


@NgModule({
  declarations: [
    DevNewsComponent,
    DevBulletinComponent,
    DevBulletinsComponent
  ],
  imports: [
    CommonModule,
    FormsModule,
    DevSecureRoutingModule,
    DevSharedModule
  ]
})
export class DevSecureModule { }
