import { NgModule } from '@angular/core';
import { RouterModule, Routes } from '@angular/router';
import {DevNewsComponent} from "./news/dev-news.component";

const routes: Routes = [
  {path:'news', component: DevNewsComponent}
];

@NgModule({
  imports: [RouterModule.forChild(routes)],
  exports: [RouterModule]
})
export class DevSecureRoutingModule { }
