import {Component, OnInit} from '@angular/core';
import {DevBulletinResponse} from "../../integration/models/response/dev-bulletin-response";
import {DevListBulletinsHttpService} from "../../integration/service/dev-list-bulletins-http.service";
import {Subscription} from "rxjs";

@Component({
  selector: 'app-news',
  templateUrl: './dev-news.component.html',
  styleUrls: ['./dev-news.component.scss']
})
export class DevNewsComponent implements OnInit {
  public bulletinList: DevBulletinResponse[];
  private _subscription: Subscription;

  constructor(
    private readonly getBulletinService: DevListBulletinsHttpService
  ) {
  }

  ngOnInit(): void {
    this._initialize();
  }

  ngOnDestroy(): void {
    this._finalize();
  }

  public getNews(): void {
    this._subscription = this.getBulletinService.getAll().subscribe((list: DevBulletinResponse[]) => {
      this.bulletinList = list;
    })
  }

  private _initialize(): void {
    this.getNews();
  }

  private _finalize() {
    this._subscription.unsubscribe();
  }
}
