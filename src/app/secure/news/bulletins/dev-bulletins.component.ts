import {Component, Input, OnInit, ChangeDetectionStrategy} from '@angular/core';
import {DevBulletinResponse} from "../../../integration/models/response/dev-bulletin-response";

@Component({
  selector: 'app-bulletins',
  templateUrl: './dev-bulletins.component.html',
  styleUrls: ['./dev-bulletins.component.scss'],
  changeDetection: ChangeDetectionStrategy.OnPush
})
export class DevBulletinsComponent implements OnInit {
  @Input() public bulletin: DevBulletinResponse;
  public fullName: string;
  public createdDate: Date;

  constructor() { }

  ngOnInit(): void {
    this._initialize();
  }

  private _initialize(): void {
    this._getInformation();
  }

  private _getInformation(): void{
    this.fullName = this.bulletin.senderUser['firstName'] + " " + this.bulletin.senderUser['lastName'];
    this.createdDate = this.bulletin.createdDate;
  }
}
