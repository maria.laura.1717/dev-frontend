import {
  ChangeDetectionStrategy,
  ChangeDetectorRef,
  Component,
  EventEmitter,
  OnInit,
  Output,
  ViewEncapsulation
} from '@angular/core';
import {NgForm} from "@angular/forms";
import {DevBulletinInput} from "../../../integration/models/input/dev-bulletin-input";
import {DevCreateBulletinHttpService} from "../../../integration/service/dev-create-bulletin-http.service";

@Component({
  selector: 'app-bulletin',
  templateUrl: './dev-bulletin.component.html',
  styleUrls: ['./dev-bulletin.component.scss'],
  encapsulation: ViewEncapsulation.None,
  changeDetection: ChangeDetectionStrategy.OnPush
})
export class DevBulletinComponent implements OnInit {
  @Output() public bulletinCreated: EventEmitter<boolean>;

  public bulletin: DevBulletinInput;

  constructor(private addBulletinHttpService: DevCreateBulletinHttpService,
              private changeDetector: ChangeDetectorRef) {
    this.bulletinCreated = new EventEmitter<boolean>();
    this.bulletin = new DevBulletinInput();
  }

  ngOnInit(): void {
  }

  public onSubmit(myForm: NgForm): void {
    this.addBulletinHttpService.add(this.bulletin).subscribe(
      resp => {
        if (resp.status === 200) {
          alert('Bulletin Added');
          this.bulletinCreated.emit(true);
          this.changeDetector.markForCheck();
        }
      },
      error => {
        if(error.status === 400){
          alert(error.error.message);
        }
      });
  }
}
