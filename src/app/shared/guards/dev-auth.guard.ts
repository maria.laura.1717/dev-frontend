import { Injectable } from '@angular/core';
import {ActivatedRouteSnapshot, CanActivate, Router, RouterStateSnapshot, UrlTree} from '@angular/router';
import { Observable } from 'rxjs';

@Injectable({
  providedIn: 'root'
})
export class DevAuthGuard implements CanActivate {
  constructor(
    private readonly router: Router
  ) {
  }
  canActivate(
    route: ActivatedRouteSnapshot,
    state: RouterStateSnapshot): boolean {
    const user_id: string = localStorage.getItem('UserId');

    if(!user_id){
      alert('Not Authorized');
      this.router.navigate(['/public/public-sign-up']);
      return false;
    }

    return true;
  }

}
