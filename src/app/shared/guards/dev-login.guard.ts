import { Injectable } from '@angular/core';
import {ActivatedRouteSnapshot, CanActivate, Router, RouterStateSnapshot, UrlTree} from '@angular/router';

@Injectable({
  providedIn: 'root'
})
export class DevLoginGuard implements CanActivate {
  constructor(
    private readonly  router:Router
  ){}

  canActivate(
    route: ActivatedRouteSnapshot,
    state: RouterStateSnapshot): boolean {

    const user_id:string =localStorage.getItem('UserId');

    if(user_id){
      this.router.navigate(['/secure/news']);
      return false;
    }
    return true;
  }

}
