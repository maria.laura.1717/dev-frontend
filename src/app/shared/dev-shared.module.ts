import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';

import { DevSearchBarComponent } from './search-bar/dev-search-bar.component';
import { DevHeaderComponent } from './header/dev-header.component';


@NgModule({
  declarations: [
    DevSearchBarComponent,
    DevHeaderComponent
  ],
  exports: [
    DevSearchBarComponent,
    DevHeaderComponent
  ],
  imports: [
    CommonModule
  ]
})
export class DevSharedModule { }
