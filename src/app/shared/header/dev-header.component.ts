import { Component, OnInit } from '@angular/core';
import {Router} from "@angular/router";

@Component({
  selector: 'app-header',
  templateUrl: './dev-header.component.html',
  styleUrls: ['./dev-header.component.scss']
})
export class DevHeaderComponent implements OnInit {

  constructor(private readonly router:Router) { }

  ngOnInit(): void {
  }

  public logOut(): void {
    let accept = confirm('Are you sure?');
    if(accept){
      localStorage.clear();
      this.router.navigate(['/public/public-sign-up']);
    }
  }
}
