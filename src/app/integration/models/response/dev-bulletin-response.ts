export interface DevBulletinResponse {
  bulletinId: number;
  senderUser: object;
  body: string;
  createdDate: Date;
}
