export interface DevUserResponse {
  userId: number;
  firstName: string;
  lastName: string;
}
