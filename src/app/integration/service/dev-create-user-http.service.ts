import { Injectable } from '@angular/core';
import {HttpClient, HttpHeaders} from "@angular/common/http";
import {DevUserInput} from "../models/input/dev-user-input";
import {Observable} from "rxjs";

@Injectable()
export class DevCreateUserHttpService {

  constructor(private http:HttpClient) {
  }

  public add(user: DevUserInput):Observable<any> {
    return this.http.post<any>('http://localhost:8081/users', user, {observe: 'response'});
  }
}
