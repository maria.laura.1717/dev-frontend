import { Injectable } from '@angular/core';
import {DevBulletinResponse} from "../models/response/dev-bulletin-response";
import {HttpClient, HttpHeaders} from "@angular/common/http";
import {Observable} from "rxjs";

@Injectable()
export class DevListBulletinsHttpService {

  constructor(private http: HttpClient) { }

  public getAll(): Observable<DevBulletinResponse[]> {
    let userId = localStorage.getItem("UserId");
    const headers = new HttpHeaders({
      "User-ID": userId,
      "Account-ID": userId
    });

    return this.http.get<DevBulletinResponse[]>('http://localhost:8090/bulletins/list',{headers: headers});
  }
}
