import {Injectable} from '@angular/core';
import {HttpClient, HttpHeaders} from "@angular/common/http";
import {DevBulletinInput} from "../models/input/dev-bulletin-input";
import {Observable} from "rxjs";

@Injectable()
export class DevCreateBulletinHttpService {

  constructor(private http: HttpClient) {
  }

  public add(bulletin: DevBulletinInput): Observable<any> {
    const headers = new HttpHeaders({
      "User-ID": this._getUser(),
      "Account-ID": this._getUser()
    });

    return this.http.post<any>('http://localhost:8090/bulletins', bulletin, {observe: 'response', headers: headers});
  }

  private _getUser(): string {
    return localStorage.getItem('UserId');
  }
}
