import { NgModule } from '@angular/core';
import { BrowserModule } from '@angular/platform-browser';

import { AppRoutingModule } from './app-routing.module';
import { AppComponent } from './app.component';
import {FormsModule} from "@angular/forms";
import {HttpClientModule} from "@angular/common/http";
import {DevCreateUserHttpService} from "./integration/service/dev-create-user-http.service";
import {DevCreateBulletinHttpService} from "./integration/service/dev-create-bulletin-http.service";
import {DevListBulletinsHttpService} from "./integration/service/dev-list-bulletins-http.service";

@NgModule({
  declarations: [
    AppComponent
  ],
  imports: [
    BrowserModule,
    FormsModule,
    HttpClientModule,
    AppRoutingModule
  ],
  providers: [DevCreateUserHttpService, DevCreateBulletinHttpService, DevListBulletinsHttpService],
  bootstrap: [AppComponent]
})
export class AppModule { }
