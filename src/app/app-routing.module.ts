import { NgModule } from '@angular/core';
import { RouterModule, Routes } from '@angular/router';
import {DevAuthGuard} from "./shared/guards/dev-auth.guard";

const routes: Routes = [
  {
    path: '', redirectTo: '/public/public-sign-up',pathMatch:'full'
  },
  {
    path: 'public',
    loadChildren: ()=> import('./public/public.module').then(m => m.PublicModule)
  },
  {
    path: 'secure',
    loadChildren: ()=> import('./secure/dev-secure.module').then(m => m.DevSecureModule),
    canActivate: [DevAuthGuard]
  }
];

@NgModule({
  imports: [RouterModule.forRoot(routes)],
  exports: [RouterModule]
})
export class AppRoutingModule { }
